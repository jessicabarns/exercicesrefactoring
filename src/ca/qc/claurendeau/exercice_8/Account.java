package ca.qc.claurendeau.exercice_8;

// Simplification de if-else enchaînés

public class Account {
	boolean isDead;
	boolean isSeparated;
	boolean isRetired;

	public double getPayAmount() {
		double result;
		if (isDead){
			result = deadAmount();
		}
		else {
			if (isSeparated){
				result = separatedAmount();
			}
			else {
				if (isRetired){
					result = retiredAmount();
				}
				else{
					result = normalPayAmount();
				}
			}
		}
		return result;
	}

	private double normalPayAmount() {
		// TODO Auto-generated method stub
		return 0;
	}

	private double retiredAmount() {
		// TODO Auto-generated method stub
		return 0;
	}

	private double separatedAmount() {
		// TODO Auto-generated method stub
		return 0;
	}

	private double deadAmount() {
		// TODO Auto-generated method stub
		return 0;
	}
}
