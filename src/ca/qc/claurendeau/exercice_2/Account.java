package ca.qc.claurendeau.exercice_2;

// Déplacement de champ

public class Account {
  // ...
  private AccountType type;
  private double interestRate;

  public double interestForAmount_days(double amount, int days) {
    return interestRate * amount * days / 365.0;
  }
}
